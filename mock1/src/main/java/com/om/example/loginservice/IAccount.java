package com.om.example.loginservice;
 
public interface IAccount {
   void setLoggedIn(boolean value);
   boolean passwordMatches(String candidate);
   void setRevoked(boolean value);
   boolean isLoggedIn();
   boolean isRevoked();
   boolean passwordExpired(String candidate);
   boolean passwordChanged(String candidate);
   boolean passwordTemporary(String candidate);
   boolean isPrevious24Password(String candidate);
   void setNewPassword(String candidate);
   void setPasswordChanged(boolean value);
}