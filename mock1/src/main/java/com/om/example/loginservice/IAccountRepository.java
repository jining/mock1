package com.om.example.loginservice;
 
public interface IAccountRepository {
    IAccount find(String accountId);
}