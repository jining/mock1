package com.om.example.loginservice;
 
public class LoginService {
   private final IAccountRepository accountRepository;
 
   public LoginService(IAccountRepository accountRepository) {
      this.accountRepository = accountRepository;
   }
 
   private int failedAttempts = 0;
   private String previousAccountId = "";
   
   
   public void login(String accountId, String password) {
      IAccount account = accountRepository.find(accountId);
 
      if (account == null)
          throw new AccountNotFoundException();
      if (account.passwordMatches(password)) {
    	  if (account.passwordExpired(password)&& !account.passwordChanged(password))
    	  	  throw new PasswordExpiredException();
    	  if (account.passwordTemporary(password)&& !account.passwordChanged(password))
    	  	  throw new PasswordTemporaryException();
    	  if (account.isLoggedIn())
              throw new AccountLoginLimitReachedException();
          if (account.isRevoked())
              throw new AccountRevokedException();
         account.setLoggedIn(true);
      } else {
         if (previousAccountId.equals(accountId))
            ++failedAttempts;
         else {
            failedAttempts = 1;
            previousAccountId = accountId;
         }
      }
 
      if (failedAttempts == 3)
         account.setRevoked(true);
   }
   public void changePassword(String accountId, String oldPassword, String newPassword){
	     IAccount account = accountRepository.find(accountId);
	      if (account == null)
	          throw new AccountNotFoundException();
	      if (account.passwordMatches(oldPassword)) {
	    	  if (account.isPrevious24Password(newPassword))
	    		  throw new PasswordPrevious24Exception();
	    	  account.setNewPassword(newPassword);
	    	  account.setPasswordChanged(true);
	      }
	     
   }
}