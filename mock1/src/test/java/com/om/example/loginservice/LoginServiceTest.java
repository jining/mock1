package com.om.example.loginservice;
 
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;

import org.junit.Before;
import org.junit.Test;
 
public class LoginServiceTest {
   private IAccount account;
   private IAccountRepository accountRepository;
   private LoginService service;
 
   @Before
   public void init() {
      account = mock(IAccount.class);
      accountRepository = mock(IAccountRepository.class);
      when(accountRepository.find(anyString())).thenReturn(account);
      service = new LoginService(accountRepository);
   }
 
   private void willPasswordMatch(boolean value) {
      when(account.passwordMatches(anyString())).thenReturn(value);
   }

//7 tests from the tutorial
   @Test
   public void itShouldSetAccountToLoggedInWhenPasswordMatches() {
      willPasswordMatch(true);
      service.login("brett", "password");
      verify(account, times(1)).setLoggedIn(true);
   }
 
   @Test
   public void itShouldSetAccountToRevokedAfterThreeFailedLoginAttempts() {
      willPasswordMatch(false);
 
      for (int i = 0; i < 3; ++i)
         service.login("brett", "password");
 
      verify(account, times(1)).setRevoked(true);
   }
   @Test
   public void itShouldNotSetAccountLoggedInIfPasswordDoesNotMatch() {
      willPasswordMatch(false);
      service.login("brett", "password");
      verify(account, never()).setLoggedIn(true);
   }
   @Test
   public void itShouldNotRevokeSecondAccountAfterTwoFailedAttemptsFirstAccount() {
      willPasswordMatch(false);
 
      IAccount secondAccount = mock(IAccount.class);
      when(secondAccount.passwordMatches(anyString())).thenReturn(false);
      when(accountRepository.find("schuchert")).thenReturn(secondAccount);
 
      service.login("brett", "password");
      service.login("brett", "password");
      service.login("schuchert", "password");
 
      verify(secondAccount, never()).setRevoked(true);
   }
   @Test(expected = AccountLoginLimitReachedException.class)
   public void itShouldNowAllowConcurrentLogins() {
      willPasswordMatch(true);
      when(account.isLoggedIn()).thenReturn(true);
      service.login("brett", "password");
   }
   @Test(expected = AccountNotFoundException.class)
   public void ItShouldThrowExceptionIfAccountNotFound() {
      when(accountRepository.find("schuchert")).thenReturn(null);
      service.login("schuchert", "password");
   }
   @Test(expected = AccountRevokedException.class)
   public void ItShouldNotBePossibleToLogIntoRevokedAccount() {
      willPasswordMatch(true);
      when(account.isRevoked()).thenReturn(true);
      service.login("brett", "password");
   }
   
//Cannot Login to Account with Expired Password
   @Test(expected = PasswordExpiredException.class)
   public void ImpossibleToLoginExpiredPassword() {
	   willPasswordMatch(true);
	   when(account.passwordExpired("password")).thenReturn(true);
	   when(account.passwordChanged("password")).thenReturn(false);
	   service.login("schuchert", "password");
	   
   }

//Can Login to Account with Expired Password After Changing the Password
   @Test
   public void LoginAfterExpiredPasswordChanged(){
	   willPasswordMatch(true);
	   when(account.passwordExpired("password")).thenReturn(true);
	   when(account.passwordChanged("password")).thenReturn(true);
	   service.login("schuchert", "password");
	   verify(account, times(1)).setLoggedIn(true);

   }
   
//Cannot Login to Account with Temporary Password
   @Test(expected = PasswordTemporaryException.class)
   public void ImpossibleToLoginTemporaryPassword() {
	   willPasswordMatch(true);
	   when(account.passwordTemporary("password")).thenReturn(true);
	   when(account.passwordChanged("password")).thenReturn(false);
	   service.login("schuchert", "password");
	   
   }
   
//Can Login to Account with Temporary Password After Changing Password
   @Test
   public void LoginAfterTemporaryPasswordChanged(){
	   willPasswordMatch(true);
	   when(account.passwordTemporary("password")).thenReturn(true);
	   when(account.passwordChanged("password")).thenReturn(true);
	   service.login("schuchert", "password");
	   verify(account, times(1)).setLoggedIn(true);

   }
   
//Cannot Change Password to any of Previous 24 passwords
   @Test(expected = PasswordPrevious24Exception.class)
   public void ImpossibleToChangeToPrevious24Password(){
	   willPasswordMatch(true);
	   when(account.isPrevious24Password("newpassword")).thenReturn(true);
	   service.changePassword("schuchert", "password", "newpassword");
   }
   
//Can Change Password to Previous password if > 24 Changes from last use
   @Test
   public void ChangeToEarlierThanPrevious24Password(){
	   willPasswordMatch(true);
	   when(account.isPrevious24Password("newpassword")).thenReturn(false);
	   service.changePassword("schuchert", "password", "newpassword");
	   verify(account, times(1)).setPasswordChanged(true);
   }
   
}